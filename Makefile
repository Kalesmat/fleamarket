# define a makefile variable for the java compiler
JCC = java
# typing 'make' will invoke the first target entry in the

all: start verify compile test package docker

# (the default one in this case)
start:
# 1. Clean
	echo "Cleaning"
	mvn validate
	mvn clean

verify:
# 2. Verify
	echo "Verifying"
	mvn verify -DskipTests

compile:
# 3. Compile the code
	echo "Compiling"
	mvn compile

test:
	mvn validate
	mvn test

package:
# 7. Package the software for release.
	echo "Packaging"
	mvn validate
	mvn package -DskipTests
	
docker:
# Create and push the docker image
	echo "building and pushing docker image"
	docker build -t flea-market:1.0.0 .
