package za.co.wethinkcode.fleamarket.db;

import org.junit.jupiter.api.Test;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Purchase;
import za.co.wethinkcode.fleamarket.app.model.Rating;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.*;

public class MemoryDBTest {

    @Test
    public void findPersonExits(){
        Person person_search = DataRepository.getInstance().findPerson("herman@wethinkcode.co.za");
        assertEquals("herman@wethinkcode.co.za", person_search.getEmail());
        assertEquals("herman", person_search.getName());
    }

    @Test
    public void findPersonNotExits(){
        Person person_search = DataRepository.getInstance().findPerson("jeff@wethinkcode.co.za");
        assertNull(person_search);
    }

    @Test
    public void findProductExits(){
        Product product = new Product("Nuts", 30.0, "product", "Food", 6
                , DataRepository.getInstance().findPerson("herman@wethinkcode.co.za"));
        Product product_search = DataRepository.getInstance().findProduct(product);
        assertNotNull(product_search);
        assertEquals(product, product_search);
    }

    @Test
    public void findProductNotExits(){
        Product product = new Product("Nuts and grape", 30.0, "product", "Food", 6
                , DataRepository.getInstance().findPerson("herman@wethinkcode.co.za"));
        Product product_search = DataRepository.getInstance().findProduct(product);
        assertNull(product_search);
    }

    @Test
    public void findProductIDExits(){
        Product product = new Product("Nuts", 30.0, "product", "Food", 6
                , DataRepository.getInstance().findPerson("herman@wethinkcode.co.za"));
        Product product_search = DataRepository.getInstance().findProduct(product);
        Product product1 = DataRepository.getInstance().findProductID(product_search.getId());
        assertNotNull(product1);
        assertEquals(product1, product_search);
    }

    @Test
    public void editProductIDExits(){
        Product product = new Product("Coke", 10.0, "product", "Drink"
                , 3, DataRepository.getInstance().findPerson("herman@wethinkcode.co.za"));
        Product product_search = DataRepository.getInstance().findProduct(product);
        Product editProduct = new Product("Sprite", 18.0, "product", "Drink"
                , 13, DataRepository.getInstance().findPerson("herman@wethinkcode.co.za"));
        DataRepository.getInstance().editProductID(product_search.getId(), editProduct);
        assertEquals("Sprite", product_search.getDescription());
        assertEquals(18.0, product_search.getPrice());
        assertEquals("product", product_search.getType());
        assertEquals("Drink", product_search.getCategory());
        assertEquals(13, product_search.getQuantity());
        assertEquals("herman@wethinkcode.co.za", product_search.getSeller().getEmail());
    }

    @Test
    public void findProductNotIDExits(){
        Product product = new Product("Sprite", 10.0, "product", "Drink"
                , 3, DataRepository.getInstance().findPerson("herman@wethinkcode.co.za"));
        Product product_search = DataRepository.getInstance().findProductID(product.getId());
        assertNull(product_search);
    }

    @Test
    public void getPersonProducts(){
        List<Product> productList = DataRepository.getInstance().getProductsPerson("herman@wethinkcode.co.za");
        assertEquals(4, productList.size());
    }

    @Test
    public void getPersonProductsNot(){
        List<Product> productList = DataRepository.getInstance().getProductsPerson("hermn@wethinkcode.co.za");
        assertNull(productList);
    }

    @Test
    public void getPersonPurchases(){
        List<Purchase> purchaseList = DataRepository.getInstance().getPurchasesPerson("mike@wethinkcode.co.za");
        assertEquals(2, purchaseList.size());
    }

    @Test
    public void getPersonPurchasesNot(){
        List<Product> productList = DataRepository.getInstance().getProductsPerson("mikel@wethinkcode.co.za");
        assertNull(productList);
    }

    @Test
    public void getTotalPersonPurchases(){
        Double total = DataRepository.getInstance().getTotalPurchasesPerson("mike@wethinkcode.co.za");
        assertEquals(40.0, total);
    }

    @Test
    public void getTotalPersonPurchasesNot(){
        Double total = DataRepository.getInstance().getTotalPurchasesPerson("mike1@wethinkcode.co.za");
        assertEquals(0.0, total);
    }

    @Test
    public void addPerson(){
        DataRepository.getInstance().addPerson(new Person("jeffk@wethinkcode.co.za"));
        assertEquals("jeffk@wethinkcode.co.za", DataRepository.getInstance().findPerson("jeffk@wethinkcode.co.za").getEmail());
    }

    @Test
    public void addPurchase(){
        Product p1 = new Product("Laptop", 10000.0, "product", "Hardware", 1, new Person("herman@wethinkcode.co.za"));
        DataRepository.getInstance().addPurchase(new Purchase(p1, 1, new Person("mike@wethinkcode.co.za")
                , LocalDate.of(2021, 9, 30)));
        Product product = DataRepository.getInstance().findProduct(p1);
        assertEquals(3, DataRepository.getInstance().findPerson("mike@wethinkcode.co.za").getPurchases().size());
        assertEquals(0, product.getQuantity());
    }

    @Test
    public void addRating(){
        Product product = new Product("Coke", 10.0, "product", "Drink"
                , 3, DataRepository.getInstance().findPerson("herman@wethinkcode.co.za"));
        DataRepository.getInstance().addRating(new Rating(product, new Person("mike@wethinkcode.co.za"), 4, "It sucked"));
        assertEquals(1, DataRepository.getInstance().findProduct(product).getRatings().size());
        assertEquals(4, DataRepository.getInstance().findProduct(product).getRating());
    }
}
