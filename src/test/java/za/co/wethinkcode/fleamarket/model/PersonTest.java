package za.co.wethinkcode.fleamarket.model;

import org.junit.jupiter.api.Test;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Purchase;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class PersonTest {

    @Test
    public void createAPerson(){
        Person person = new Person("lmatsabu@student.wethinkcode.co.za");
        assertEquals("lmatsabu@student.wethinkcode.co.za", person.getEmail());
        assertEquals("lmatsabu", person.getName());
    }

    @Test
    public void addAGood(){
        Person person = new Person("lmatsabu@student.wethinkcode.co.za");
        Product good = new Product("Coke", 10.0, "product", "Drink", 3, person);
        person.addGoods(good);
        assertEquals(1, person.getGoods().size());
    }

    @Test
    public void addAPurchase(){
        Person person = new Person("stevet@student.wethinkcode.co.za");
        Product good = new Product("Coke", 10.0, "product", "Drink", 3, person);
        person.addGoods(good);
        Person person2 = new Person("lmatsabu@student.wethinkcode.co.za");
        Purchase purchase = new Purchase(good, 1, person2, LocalDate.of(2021, 9, 30));
        person2.addPurchase(purchase);
        assertEquals(1, person2.getPurchases().size());
    }

    @Test
    public void totalPurchase(){
        Person person = new Person("stevet@student.wethinkcode.co.za");
        Product good = new Product("Coke", 10.0, "product", "Drink", 3, person);
        person.addGoods(good);
        Person person2 = new Person("lmatsabu@student.wethinkcode.co.za");
        Purchase purchase = new Purchase(good, 2, person2, LocalDate.of(2021, 9, 30));
        person2.addPurchase(purchase);
        assertEquals(20.0, person2.getTotalPurchases());
    }
}
