package za.co.wethinkcode.fleamarket.model;

import org.junit.jupiter.api.Test;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Purchase;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class PurchaseTest {

    @Test
    public void createAPurchase(){
        Person person = new Person("stevet@student.wethinkcode.co.za");
        Product good = new Product("Coke", 10.0, "product", "Drink", 3, person);
        person.addGoods(good);
        Person person2 = new Person("lmatsabu@student.wethinkcode.co.za");
        Purchase purchase = new Purchase(good, 1, person2, LocalDate.of(2021, 9, 30));
        assertTrue(purchase.getProduct().equals(good));
        assertEquals(1, purchase.getQuantity());
        assertTrue(purchase.getBuyer().equals(person2));
        assertEquals(LocalDate.of(2021, 9, 30), purchase.getDate());
        assertEquals(10.0, purchase.getTotal_amount());
    }

    @Test
    public void totalAmountTrue(){
        Person person = new Person("stevet@student.wethinkcode.co.za");
        Product good = new Product("Coke", 10.0, "product", "Drink", 3, person);
        person.addGoods(good);
        Person person2 = new Person("lmatsabu@student.wethinkcode.co.za");
        Purchase purchase = new Purchase(good, 2, person2, LocalDate.of(2021, 9, 30));
        assertEquals(20.0, purchase.getTotal_amount());
    }
}
