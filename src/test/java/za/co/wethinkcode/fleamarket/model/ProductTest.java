package za.co.wethinkcode.fleamarket.model;

import org.junit.jupiter.api.Test;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Rating;

import static org.junit.jupiter.api.Assertions.*;

public class ProductTest {

    @Test
    public void changeAProduct(){
        Person person = new Person("lmatsabu@student.wethinkcode.co.za");
        Product product = new Product("Sprite", 10.0, "product", "Drink", 3, person);
        product.setProduct("Sprite", 18.0, "product", "Drink", 30);
        assertEquals("Sprite", product.getDescription());
        assertEquals(18.0, product.getPrice());
        assertEquals("product", product.getType());
        assertEquals("Drink", product.getCategory());
        assertEquals(30, product.getQuantity());
        assertEquals("lmatsabu@student.wethinkcode.co.za", product.getSeller().getEmail());
        assertEquals(0, product.getRating());
    }

    @Test
    public void createAProduct(){
        Person person = new Person("lmatsabu@student.wethinkcode.co.za");
        Product product = new Product("Coke", 10.0, "product", "Drink", 3, person);
        assertEquals("Coke", product.getDescription());
        assertEquals(10.0, product.getPrice());
        assertEquals("product", product.getType());
        assertEquals("Drink", product.getCategory());
        assertEquals(3, product.getQuantity());
        assertEquals("lmatsabu@student.wethinkcode.co.za", product.getSeller().getEmail());
        assertEquals(0, product.getRating());
    }

    @Test
    public void addARating(){
        Person person = new Person("lmatsabu@student.wethinkcode.co.za");
        Product good = new Product("Coke", 10.0, "product", "Drink", 3, person);
        Person person2 = new Person("gregt@student.wethinkcode.co.za");
        Rating rating = new Rating(good, person2, 5, " ");
        good.addRating(rating);
        assertEquals(1, good.getRatings().size());
        assertEquals(5, good.getRating());
    }

    @Test
    public void averageRating(){
        Person person = new Person("lmatsabu@student.wethinkcode.co.za");
        Product good = new Product("Coke", 10.0, "product", "Drink", 3, person);
        Person person2 = new Person("gregt@student.wethinkcode.co.za");
        Rating rating = new Rating(good, person2, 5, " ");
        good.addRating(rating);
        Person person3 = new Person("jasonm@student.wethinkcode.co.za");
        Rating rating2 = new Rating(good, person3, 1, "It sucks");
        good.addRating(rating2);
        assertEquals(2, good.getRatings().size());
        assertEquals(3, good.getRating());
    }

    @Test
    public void buyAProduct(){
        Person person = new Person("lmatsabu@student.wethinkcode.co.za");
        Product product = new Product("Coke", 10.0, "product", "Drink", 8, person);
        assertEquals(8, product.getQuantity());
        product.subtractQuantity(4);
        assertEquals(4, product.getQuantity());
    }
}
