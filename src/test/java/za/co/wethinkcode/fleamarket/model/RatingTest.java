package za.co.wethinkcode.fleamarket.model;

import org.junit.jupiter.api.Test;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Rating;

import static org.junit.jupiter.api.Assertions.*;

public class RatingTest {

    @Test
    public void createARating(){
        Person person = new Person("lmatsabu@student.wethinkcode.co.za");
        Product good = new Product("Coke", 10.0, "product", "Drink", 3, person);
        Person person2 = new Person("gregt@student.wethinkcode.co.za");
        Rating rating = new Rating(good, person2, 5, "Good Product");
        assertEquals(good, rating.getProduct());
        assertEquals(person2, rating.getRater());
        assertEquals(5, rating.getRating());
        assertEquals("Good Product", rating.getComment());
    }
}
