package za.co.wethinkcode.fleamarket.webapi.Controllers;

import io.javalin.http.Context;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;

import java.util.HashMap;

public class MyProductsController {

    public static final String PATH = "/myProducts";

    public static void renderProductList(Context context){
        HashMap<String, Object> viewModel = new HashMap<>();

        Person user = context.sessionAttribute("user");

        var Products = DataRepository.getInstance().getProductsPerson(user.getEmail());

        viewModel.put("products", Products);

        context.render("myProducts.html", viewModel);
    }
}
