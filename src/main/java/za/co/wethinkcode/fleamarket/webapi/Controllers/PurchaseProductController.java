package za.co.wethinkcode.fleamarket.webapi.Controllers;

import io.javalin.http.Context;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Purchase;
import za.co.wethinkcode.fleamarket.app.model.Rating;

import java.time.LocalDate;
import java.util.Objects;

public class PurchaseProductController {
    public static final String PATH = "/buy";
    public static final String CAPTURE_PATH = "/createPurchase";
    public static final String ROOT_PATH = "/purchaseForm.html";

    public static void createPurchase(Context context){

        Person person = context.sessionAttribute("user");
        Product product = context.sessionAttribute("product");
        int quantity = Integer.parseInt(Objects.requireNonNull(context.formParam("quantity")));

        assert product != null;
        DataRepository.getInstance().addPurchase(new Purchase(product,quantity,person, LocalDate.now()));

        context.redirect(ViewAllProductsController.PATH);
    }

    public static void renderForm(Context context){
        context.render(ROOT_PATH);
    }
}
