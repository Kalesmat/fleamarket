package za.co.wethinkcode.fleamarket.webapi.Controllers;

import io.javalin.http.Context;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

public class ProductDetailsController {
    public static final String PATH = "/product";

    public static void renderProductPage(Context context){
        HashMap<String, Object> viewModel = new HashMap<>();

        UUID productId = UUID.fromString(Objects.requireNonNull(context.queryParam("productId")));
        var product = DataRepository.getInstance().findProductID(productId);
        var ratings = product.getRatings();

        context.sessionAttribute("product", product);
        viewModel.put("product", product);
        viewModel.put("ratings", ratings);
        context.render("product.html", viewModel);
    }
}
