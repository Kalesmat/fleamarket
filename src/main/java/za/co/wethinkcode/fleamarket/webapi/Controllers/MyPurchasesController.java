package za.co.wethinkcode.fleamarket.webapi.Controllers;

import io.javalin.http.Context;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;

import java.util.HashMap;

public class MyPurchasesController {

    public static final String PATH = "/myPurchases";

    public static void renderPurchaseList(Context context){
        HashMap<String, Object> viewModel = new HashMap<>();

        Person user = context.sessionAttribute("user");

        var purchases = DataRepository.getInstance().getPurchasesPerson(user.getEmail());

        viewModel.put("purchases", purchases);

        context.render("myPurchase.html", viewModel);
    }
}
