package za.co.wethinkcode.fleamarket.webapi.Controllers;

import io.javalin.http.Context;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Rating;

import java.util.Objects;

public class RateProductController {
    public static final String PATH = "/newRating";
    public static final String CAPTURE_PATH = "/createRating";
    public static final String ROOT_PATH = "/ratingForm.html";

    public static void createRating(Context context){

        Person person = context.sessionAttribute("user");
        Product product = context.sessionAttribute("product");
        int score = Integer.parseInt(Objects.requireNonNull(context.formParam("score")));
        String comment = context.formParam("comment");

        DataRepository.getInstance().addRating(new Rating(product,person,score,comment));

        context.redirect(ViewAllProductsController.PATH);
    }

    public static void renderForm(Context context){
        context.render(ROOT_PATH);
    }
}
