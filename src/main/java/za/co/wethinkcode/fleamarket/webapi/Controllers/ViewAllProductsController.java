package za.co.wethinkcode.fleamarket.webapi.Controllers;

import io.javalin.http.Context;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;

import java.util.HashMap;

public class ViewAllProductsController {

    public static final String PATH = "/home";

    public static void renderHomePage(Context context){
        HashMap<String, Object> viewModel = new HashMap<>();

        Person user = context.sessionAttribute("user");

        var Products = DataRepository.getInstance().getProducts();

        viewModel.put("products", Products);

        context.render("home.html", viewModel);
    }
}
