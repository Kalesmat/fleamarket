package za.co.wethinkcode.fleamarket.webapi;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import io.javalin.plugin.rendering.template.JavalinThymeleaf;
import nz.net.ultraq.thymeleaf.layoutdialect.LayoutDialect;
import org.jetbrains.annotations.NotNull;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import za.co.wethinkcode.fleamarket.app.DefaultAccessManager;
import za.co.wethinkcode.fleamarket.app.Sessions;
import za.co.wethinkcode.fleamarket.webapi.Controllers.*;

import static io.javalin.apibuilder.ApiBuilder.*;

public class FleaMarketServer {
    private static final int DEFAULT_PORT = 8080;
    private static final String STATIC_DIR = "/html";

    /**
     * The main class starts the server on the default port 7070.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        Javalin app = getInstance();
        app.start(DEFAULT_PORT);
    }

    /**
     * This is a convenience for running Selenium tests.
     * It allows the test to get access to the server to start and stop it.
     * @return a configured server for the app
     */
    public static Javalin getInstance() {
        configureThymeleafTemplateEngine();
        Javalin server = createAndConfigureServer();
        setupRoutes(server);
        return server;
    }

    /**
     * Setup the Thymeleaf template engine to load templates from 'resources/templates'
     */
    private static void configureThymeleafTemplateEngine() {
        TemplateEngine templateEngine = new TemplateEngine();

        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/templates/");
        templateEngine.setTemplateResolver(templateResolver);

        templateEngine.addDialect(new LayoutDialect());
        JavalinThymeleaf.configure(templateEngine);
    }

    private static void setupRoutes(Javalin server) {
        server.routes(() -> {
            loginAndLogoutRoutes();
            homePageRoute();
            myProductsRoutes();
            createProductRoutes();
            editProductRoutes();
            productDetailRoutes();
            createRatingRoutes();
            createPurchaseRoutes();
            myPurchasesRoutes();
        });
    }

    private static void homePageRoute() {
        path(ViewAllProductsController.PATH, () -> get(ViewAllProductsController::renderHomePage));
    }

    private static void loginAndLogoutRoutes() {
        post(LoginController.LOGIN_PATH, LoginController::handleLogin);
        get(LoginController.LOGOUT_PATH, LoginController::handleLogout);
    }

    private static void myProductsRoutes() {
        path(MyProductsController.PATH, () -> get(MyProductsController::renderProductList));
    }

    private static void myPurchasesRoutes() {
        path(MyPurchasesController.PATH, () -> get(MyPurchasesController::renderPurchaseList));
    }

    private static void productDetailRoutes() {
        path(ProductDetailsController.PATH, () -> get(ProductDetailsController::renderProductPage));
    }

    private static void createProductRoutes() {
        path(CreateProductController.PATH, () -> get(CreateProductController::renderForm));
        path(CreateProductController.CAPTURE_PATH, () -> post(CreateProductController::createProduct));
    }

    private static void editProductRoutes() {
        path(EditProductController.PATH, () -> get(EditProductController::renderForm));
        path(EditProductController.CAPTURE_PATH, () -> post(EditProductController::editProduct));
    }

    private static void createRatingRoutes() {
        path(RateProductController.PATH, () -> get(RateProductController::renderForm));
        path(RateProductController.CAPTURE_PATH, () -> post(RateProductController::createRating));
    }

    private static void createPurchaseRoutes() {
        path(PurchaseProductController.PATH, () -> get(PurchaseProductController::renderForm));
        path(PurchaseProductController.CAPTURE_PATH, () -> post(PurchaseProductController::createPurchase));
    }

    @NotNull
    private static Javalin createAndConfigureServer() {
        return Javalin.create(config -> {
            config.addStaticFiles(STATIC_DIR, Location.CLASSPATH);
            config.sessionHandler(Sessions::nopersistSessionHandler);
            config.accessManager(new DefaultAccessManager());
        });
    }
}
