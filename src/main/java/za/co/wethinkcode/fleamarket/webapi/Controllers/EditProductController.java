package za.co.wethinkcode.fleamarket.webapi.Controllers;

import io.javalin.http.Context;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;

import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

public class EditProductController {

    public static final String PATH = "/changeProduct";
    public static final String CAPTURE_PATH = "/editProduct";
    public static final String ROOT_PATH = "/editForm.html";

    public static void editProduct(Context context){

        String description = context.formParam("description");
        double price = Double.parseDouble(Objects.requireNonNull(context.formParam("price")));
        String type = context.formParam("type");
        String category = context.formParam("category");
        int quantity = Integer.parseInt(Objects.requireNonNull(context.formParam("quantity")));
        Person person = context.sessionAttribute("user");
        Product product = context.sessionAttribute("product");

        assert product != null;
        DataRepository.getInstance().editProductID(product.getId(), new Product(description, price, type
                , category, quantity, person));

        context.redirect(MyProductsController.PATH);
    }

    public static void renderForm(Context context){
        HashMap<String, Object> viewModel = new HashMap<>();
        UUID productId = UUID.fromString(Objects.requireNonNull(context.queryParam("productId")));
        var product = DataRepository.getInstance().findProductID(productId);
        context.sessionAttribute("product", product);
        viewModel.put("product", product);
        context.render(ROOT_PATH, viewModel);
    }
}
