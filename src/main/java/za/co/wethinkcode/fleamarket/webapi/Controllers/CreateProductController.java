package za.co.wethinkcode.fleamarket.webapi.Controllers;

import io.javalin.http.Context;
import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;

public class CreateProductController {
    public static final String PATH = "/newProduct";
    public static final String CAPTURE_PATH = "/createProduct";
    public static final String ROOT_PATH = "/productForm.html";

    public static void createProduct(Context context){

        String description = context.formParam("description");
        double price = Double.parseDouble(context.formParam("price"));
        String type = context.formParam("type");
        String category = context.formParam("category");
        int quantity = Integer.parseInt(context.formParam("quantity"));
        Person person = context.sessionAttribute("user");

        DataRepository.getInstance().addProduct(new Product(description, price, type
                , category, quantity, person));

        context.redirect(MyProductsController.PATH);
    }

    public static void renderForm(Context context){
        context.render(ROOT_PATH);
    }
}
