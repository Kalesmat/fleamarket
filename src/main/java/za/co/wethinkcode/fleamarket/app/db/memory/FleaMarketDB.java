package za.co.wethinkcode.fleamarket.app.db.memory;

import za.co.wethinkcode.fleamarket.app.db.DataRepository;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Purchase;
import za.co.wethinkcode.fleamarket.app.model.Rating;

import java.time.LocalDate;
import java.util.*;

public class FleaMarketDB implements DataRepository {

    private final List<Person> people = new ArrayList<>();
    private final List<Product> products = new ArrayList<>();
    private final List<Purchase> purchases = new ArrayList<>();
    private final List<Rating> ratings = new ArrayList<>();

    public FleaMarketDB() {
        setTestData();
    }

    @Override
    public Person findPerson(String email) {
        for(Person p: people){
            if(p.getEmail().equalsIgnoreCase(email)){
                return p;
            }
        }
        return null;
    }

    @Override
    public Product findProductID(UUID productID) {
        for(Product p: products){
            if(p.getId().equals(productID)){
                return p;
            }
        }
        return null;
    }

    @Override
    public void editProductID(UUID productID, Product newProduct) {
        findProductID(productID).setProduct(newProduct.getDescription(), newProduct.getPrice()
                , newProduct.getType(), newProduct.getCategory(), newProduct.getQuantity());
    }

    @Override
    public Product findProduct(Product product) {
        for(Product p: products){
            if(p.equals(product)){
                return p;
            }
        }
        return null;
    }

    @Override
    public List<Product> getProductsPerson(String email){
        if(findPerson(email) != null) {
            return findPerson(email).getGoods();
        }
        return null;
    }

    @Override
    public Person addPerson(Person person){
        if(findPerson(person.getEmail()) == null) {
            people.add(person);
        }
        return person;
    }

    @Override
    public Product addProduct(Product product){
        if(findProduct(product) == null) {
            products.add(product);
            findPerson(product.getSeller().getEmail()).addGoods(product);
        }
        return product;
    }

    @Override
    public Purchase addPurchase(Purchase purchase){
        findProduct(purchase.getProduct()).subtractQuantity(purchase.getQuantity());
        purchases.add(purchase);
        findPerson(purchase.getBuyer().getEmail()).addPurchase(purchase);
        return purchase;
    }

    @Override
    public Rating addRating(Rating rating){
        ratings.add(rating);
        findProduct(rating.getProduct()).addRating(rating);
        return rating;
    }

    @Override
    public Person removePerson(Person person){
        people.remove(person);
        return person;
    }

    @Override
    public Product removeProduct(Product product){
        products.remove(product);
        return product;
    }

    @Override
    public Purchase removePurchase(Purchase purchase){
        purchases.remove(purchase);
        return purchase;
    }

    @Override
    public Rating removeRating(Rating rating){
        ratings.remove(rating);
        return rating;
    }

    @Override
    public double getTotalPurchasesPerson(String email){
        if(findPerson(email) != null) {
            return findPerson(email).getTotalPurchases();
        }
        return 0;
    }

    @Override
    public List<Purchase> getPurchasesPerson(String email){
        if(findPerson(email) != null) {
            return findPerson(email).getPurchases();
        }
        return null;
    }

    @Override
    public List<Product> getProducts(){
        return products;
    }

    private void setTestData(){
        Person herman = addPerson(new Person("herman@wethinkcode.co.za"));
        Person mike = addPerson(new Person("mike@wethinkcode.co.za"));
        Person sett = addPerson(new Person("sett@wethinkcode.co.za"));

        Product p1 = addProduct(new Product("Coke", 10.0, "product", "Drink", 3, herman));
        Product p2 = addProduct(new Product("Nuts", 30.0, "product", "Food", 6, herman));
        Product p3 = addProduct(new Product("Laptop", 10000.0, "product", "Hardware", 1, herman));
        Product p4 = addProduct(new Product("OOP Workshop", 500.0, "service", "Java", 2, herman));

        addPurchase(new Purchase(p1, 1, mike, LocalDate.of(2021, 9, 30)));
        addPurchase(new Purchase(p2, 1, mike, LocalDate.of(2021, 10, 30)));

        addRating(new Rating(p3, mike, 5, ""));
        addRating(new Rating(p3, sett, 1, "It sucked!"));
        addRating(new Rating(p4, mike, 4, "It was informative"));

    }
}
