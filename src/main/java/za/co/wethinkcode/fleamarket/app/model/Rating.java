package za.co.wethinkcode.fleamarket.app.model;

import java.util.UUID;

public class Rating extends AbstractTransactionModelBase{

    private Product product;
    private Person rater;
    private int rating;
    private String comment;

    public Rating(Product product, Person rater, int rating, String comment) {
        super(UUID.randomUUID());
        this.product = product;
        this.rater = rater;
        this.rating = rating;
        this.comment = comment;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Person getRater() {
        return rater;
    }

    public void setRater(Person rater) {
        this.rater = rater;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Rating{" +
                "product=" + product +
                ", rater=" + rater +
                ", rating=" + rating +
                ", comment='" + comment + '\'' +
                '}';
    }
}
