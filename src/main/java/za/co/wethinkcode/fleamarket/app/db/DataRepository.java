package za.co.wethinkcode.fleamarket.app.db;

import za.co.wethinkcode.fleamarket.app.db.memory.FleaMarketDB;
import za.co.wethinkcode.fleamarket.app.model.Person;
import za.co.wethinkcode.fleamarket.app.model.Product;
import za.co.wethinkcode.fleamarket.app.model.Purchase;
import za.co.wethinkcode.fleamarket.app.model.Rating;

import java.util.*;

public interface DataRepository {

    DataRepository INSTANCE = new FleaMarketDB();

    static DataRepository getInstance(){
        return INSTANCE;
    }

    public Person findPerson(String email);

    public Product findProduct(Product product);

    public Product findProductID(UUID productID);

    public void editProductID(UUID productID, Product newProduct);

    public List<Product> getProductsPerson(String email);

    public List<Purchase> getPurchasesPerson(String email);

    public Person addPerson(Person person);

    public Product addProduct(Product product);

    public Purchase addPurchase(Purchase purchase);

    public Rating addRating(Rating rating);

    public Person removePerson(Person person);

    public Product removeProduct(Product product);

    public Purchase removePurchase(Purchase purchase);

    public Rating removeRating(Rating rating);

    public double getTotalPurchasesPerson(String email);

    public List<Product> getProducts();
}
