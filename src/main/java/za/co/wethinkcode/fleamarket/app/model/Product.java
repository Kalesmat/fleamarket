package za.co.wethinkcode.fleamarket.app.model;

import java.util.*;

public class Product  extends AbstractTransactionModelBase {

    private String description;
    private double price;
    private String type;
    private String category;
    private int quantity;
    private Person seller;
    private int rating;
    private List<Rating> ratings;

    public Product(String description, double price, String type, String category, int quantity, Person seller) {
        super(UUID.randomUUID());
        this.description = description;
        this.price = price;
        this.type = type;
        this.category = category;
        this.quantity = quantity;
        this.seller = seller;
        this.rating = 0;
        this.ratings = new ArrayList<>();
    }

    public void setProduct(String description, double price, String type, String category, int quantity){
        this.description = description;
        this.price = price;
        this.type = type;
        this.category = category;
        this.quantity = quantity;
    }

    public Rating addRating(Rating rate){
        ratings.add(rate);
        calculateAverage();
        return rate;
    }

    public void subtractQuantity(int purchase_quantity){
        this.quantity = this.quantity - purchase_quantity;
    }

    private void calculateAverage(){
        rating = ratings.stream().map(Rating::getRating).reduce(Integer::sum).get() / ratings.size();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Person getSeller() {
        return seller;
    }

    public void setSeller(Person seller) {
        this.seller = seller;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<Rating> getRatings(){
        return ratings;
    }

    @Override
    public String toString() {
        return "Product{" +
                "description='" + description + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", category='" + category + '\'' +
                ", quantity=" + quantity +
                ", seller=" + seller +
                ", rating=" + rating +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 && Objects.equals(description, product.description) && Objects.equals(type, product.type) && Objects.equals(category, product.category) && Objects.equals(seller.getEmail(), product.seller.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, price, type, category, quantity, seller, rating);
    }
}
