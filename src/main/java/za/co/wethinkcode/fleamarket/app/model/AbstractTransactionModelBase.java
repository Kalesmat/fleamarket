package za.co.wethinkcode.fleamarket.app.model;

import java.util.UUID;

import static com.google.common.base.Preconditions.checkNotNull;

public class AbstractTransactionModelBase {

    protected final UUID id;

    public AbstractTransactionModelBase(UUID tableId ){
        this.id = checkNotNull( tableId );
    }

    public UUID getId() {
        return id;
    }

}