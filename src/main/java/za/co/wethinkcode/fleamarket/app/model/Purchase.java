package za.co.wethinkcode.fleamarket.app.model;

import java.time.LocalDate;
import java.util.UUID;

public class Purchase extends AbstractTransactionModelBase{

    private Product product;
    private double total_amount;
    private int quantity;
    private Person buyer;
    private LocalDate date;

    public Purchase(Product product, int quantity, Person buyer, LocalDate date) {
        super(UUID.randomUUID());
        this.product = product;
        this.total_amount = calculateAmount(quantity, product.getPrice());
        this.quantity = quantity;
        this.buyer = buyer;
        this.date = date;
    }

    private double calculateAmount(int qt, double price){
        return qt * price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(double total_amount) {
        this.total_amount = total_amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Person getBuyer() {
        return buyer;
    }

    public void setBuyer(Person buyer) {
        this.buyer = buyer;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Purchase{" +
                "product=" + product +
                ", total_amount=" + total_amount +
                ", quantity=" + quantity +
                ", buyer=" + buyer +
                ", date=" + date +
                '}';
    }
}
