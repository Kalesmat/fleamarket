package za.co.wethinkcode.fleamarket.app.model;

import java.util.*;

/**
 * I represent a person who participates in sharing expenses. I am identified by an email address which
 * uniquely identifies me. (No other Persons may use the same email address.)
 * <p>
 * My sole responsibility is to identify an expense participant.
 */
public class Person {

    private final String email;
    private Long id;
    private List<Product> goods;
    private List<Purchase> purchases;

    public Person(String email ){
        goods = new ArrayList<>();
        purchases = new ArrayList<>();
        this.email = email;
    }

    public Product addGoods(Product product){
        goods.add(product);
        return product;
    }

    public Purchase addPurchase(Purchase purchase){
        purchases.add(purchase);
        return purchase;
    }

    public List<Product> getGoods() {
        return goods;
    }

    public List<Purchase> getPurchases(){
        return purchases;
    }

    public Double getTotalPurchases() {
        if (purchases.isEmpty()) return 0.0;
        return purchases.stream().map(Purchase::getTotal_amount).reduce(Double::sum).get();
    }

    public String getEmail(){
        return email;
    }

    @Override
    public String toString(){
        return "Person{" +
            "id='" + email + '\'' +
            '}';
    }

    @Override
    public boolean equals( Object o ){
        if( this == o ) return true;
        if( o == null || getClass() != o.getClass() ) return false;
        Person person = (Person) o;
        return email.equalsIgnoreCase( person.email );
    }

    @Override
    public int hashCode(){
        return Objects.hash( email );
    }

    public String getName(){
        String pseudonym = this.email.split( "@" )[ 0 ];
        return pseudonym;
    }

    public Long getId(){
        return id;
    }

    public void setId( Long id ){
        this.id = id;
    }
}