FROM ubuntu:21.04

RUN apt-get update

RUN apt-get install -y openjdk-11-jre curl make maven

RUN mkdir thefleamarket

WORKDIR /thefleamarket

COPY target/FleaMarket-1.0.jar /thefleamarket

EXPOSE 7000

CMD ["java", "-jar", "FleaMarket-1.0.jar"]
